module.exports = {
  "env": {
    "browser": true,
    "commonjs": true,
    "es2021": true,
    "node": true
  },
  "extends": "eslint:recommended",
  "ignorePatterns": ["test/*", ".eslintrc.js"],
  "parserOptions": {
    "ecmaVersion": 7
  },
  "rules": {
  }
};
